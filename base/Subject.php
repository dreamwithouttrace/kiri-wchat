<?php


namespace wchat\base;


use Kiri\Client;
use wchat\common\Multiprogramming;
use wchat\common\Result;

/**
 * Class Subject
 * @package wchat\base
 */
abstract class Subject extends Multiprogramming
{


    private array  $keywords         = [];
    private string $templateId       = '';
    private string $openId           = '';
    private string $page             = 'pages/index/index';
    private string $emphasis_keyword = '';
    private string $oac_appid        = '';
    private string $use_robot        = '';

    /**
     * @param array $keywords
     */
    public function setKeywords(array $keywords): void
    {
        $this->keywords = $keywords;
    }

    /**
     * @param $templateId
     */
    public function setTemplateId($templateId): void
    {
        $this->templateId = $templateId;
    }

    /**
     * @param $openId
     */
    public function setOpenId($openId): void
    {
        $this->openId = $openId;
    }

    /**
     * @param $page
     */
    public function setPage($page): void
    {
        $this->page = $page;
    }

    /**
     * @param $emphasis_keyword
     */
    public function setEmphasisKeyword($emphasis_keyword): void
    {
        $this->emphasis_keyword = $emphasis_keyword;
    }

    /**
     * @param $index
     * @param $context
     * @param string $color
     */
    public function replaceKeyword($index, $context, string $color = ''): void
    {
        if (empty($color)) {
            $color = '#000';
        }
        $this->keywords['keyword' . $index] = [
            'value' => $context,
            'color' => $color
        ];
    }


    /**
     * @param $color
     * @param $context
     */
    public function addKeyword($context, $color = null): void
    {
        if (empty($color)) {
            $color = '#000';
        }
        $this->keywords['keyword' . (count($this->keywords) + 1)] = [
            'value' => $context,
            'color' => $color
        ];
    }

    abstract public function getUrl();

    abstract public function getHost();

    /**
     * @return string
     */
    public function getOacAppid(): string
    {
        return $this->oac_appid;
    }

    /**
     * @param string $oac_appid
     */
    public function setOacAppid(string $oac_appid): void
    {
        $this->oac_appid = $oac_appid;
    }

    /**
     * @return string
     */
    public function getUseRobot(): string
    {
        return $this->use_robot;
    }

    /**
     * @param string $use_robot
     */
    public function setUseRobot(string $use_robot): void
    {
        $this->use_robot = $use_robot;
    }


    /**
     * @return Result
     * @throws
     */
    public function sendTemplate(): Result
    {
        $access_token = $this->payConfig->getAccessToken();
        if (empty($access_token)) {
            throw new \Exception('request access_token con\'t null.');
        }
        $params = [
            "touser"      => $this->openId,
            "template_id" => $this->templateId,
            "page"        => $this->page,
            "data"        => $this->keywords,
        ];
        if (!empty($this->emphasis_keyword)) {
            $params['emphasis_keyword'] = $this->emphasis_keyword;
        }
        if (!empty($this->oac_appid)) {
            $params['oac_appid'] = $this->oac_appid;
        }
        if (!empty($this->use_robot)) {
            $params['use_robot'] = $this->use_robot;
        }
        $this->reset();

        $client = new Client($this->getHost(), 443, true);
        $client->withHeader(['Content-Type' => 'application/json; charset=utf-8']);

        $proxyHost = $this->payConfig->getProxyHost();
        $proxyPort = $this->payConfig->getProxyPort();
        if (!empty($proxyHost) && $proxyPort > 0) {
            $client->withProxyHost($proxyHost)->withProxyPort($proxyPort);
        }

        $client->post($this->getUrl() . '?access_token=' . $access_token, $params);
        $client->close();

        if (!in_array($client->getStatusCode(), [101, 200, 201])) {
            return new Result(code: 505, message: $client->getBody());
        }
        $body = json_decode($client->getBody(), true);
        if (isset($body['errcode']) && $body['errcode'] != 0) {
            return new Result(code: $body['errcode'], message: $body['errmsg']);
        } else {
            return new Result(code: 0, data: $body);
        }
    }

    /**
     * @return void
     */
    public function reset(): void
    {
        $this->openId     = '';
        $this->keywords   = [];
        $this->templateId = '';
        $this->page       = '';
    }

}
