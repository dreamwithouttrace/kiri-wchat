<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/4/19 0019
 * Time: 16:12
 */

namespace wchat\wx;

use wchat\common\Result;

class Account extends SmallProgram
{

    /**
     * @param string $code
     * @return Result
     */
    public function login(string $code): Result
    {
        $param['appid']      = $this->payConfig->appId;
        $param['secret']     = $this->payConfig->appSecret;
        $param['js_code']    = $code;
        $param['grant_type'] = 'authorization_code';

        return $this->get('api.weixin.qq.com', '/sns/jscode2session', $param);
    }

    /**
     * @param string $code
     * @return Result
     */
    public function AppLogin(string $code): Result
    {
        $param['appid']      = $this->payConfig->pay->wx->appId;
        $param['secret']     = $this->payConfig->pay->wx->appSecret;
        $param['js_code']    = $code;
        $param['grant_type'] = 'authorization_code';

        return $this->get('api.weixin.qq.com', '/sns/oauth2/access_token', $param);
    }

    /**
     * @param string $openid
     * @return Result
     */
    public function getPublicUserInfo(string $openid): Result
    {
        $query = [
            'access_token' => $this->payConfig->getAccessToken(),
            'openid'       => $openid,
            'lang'         => 'zh_CN'
        ];
        return $this->get('api.weixin.qq.com', '/cgi-bin/user/info', $query);
    }

    /**
     * @param string $openid
     * @return Result
     */
    public function getAppUserInfo(string $openid): Result
    {
        $query = [
            'access_token' => $this->payConfig->getAccessToken(),
            'openid'       => $openid,
        ];
        return $this->get('api.weixin.qq.com', '/sns/userinfo', $query);
    }


    /**
     * @param string $path
     * @param int $width
     * @return array|mixed|Result
     */
    public function createwxaqrcode(string $path, int $width): mixed
    {
        $url               = 'cgi-bin/wxaapp/createwxaqrcode?access_token=';
        $sendBody['path']  = $path;
        $sendBody['width'] = $width;

        return $this->get('api.weixin.qq.com', $url . $this->payConfig->getAccessToken(), $sendBody);
    }


    /**
     * @param string $path
     * @param int $width
     * @param bool $is_hyaline
     * @param bool $auto_color
     * @param string $line_color
     * @return Result
     */
    public function getwxacode(string $path, int $width, bool $is_hyaline = false, bool $auto_color = false, string $line_color = ''): Result
    {
        $sendBody['path']       = $path;
        $sendBody['width']      = $width;
        $sendBody['auto_color'] = $auto_color;
        $sendBody['is_hyaline'] = $is_hyaline;
        $url                    = 'wxa/getwxacode?access_token=' . $this->payConfig->getAccessToken();
        if ($auto_color) {
            $sendBody['line_color'] = $line_color;
        }
        return $this->get('api.weixin.qq.com', $url . $this->payConfig->getAccessToken(), $sendBody);

    }


    /**
     * @param string $path
     * @param int $width
     * @param bool $is_hyaline
     * @param bool $auto_color
     * @param string $line_color
     * @return Result
     */
    public function getwxacodeunlimit(string $path, int $width, bool $is_hyaline = false, bool $auto_color = false, string $line_color = ''): Result
    {
        $sendBody['path']       = $path;
        $sendBody['width']      = $width;
        $sendBody['auto_color'] = $auto_color;
        $sendBody['is_hyaline'] = $is_hyaline;
        $url                    = 'wxa/getwxacodeunlimit?access_token=' . $this->payConfig->getAccessToken();
        if ($auto_color) {
            $sendBody['line_color'] = $line_color;
        }
        return $this->get('api.weixin.qq.com', $url . $this->payConfig->getAccessToken(), $sendBody);
    }

}
