<?php

namespace wchat\wx\V3;

use Exception;
use JetBrains\PhpStorm\ArrayShape;
use wchat\wx\SmallProgram;

class WxV3NativePayment extends SmallProgram
{


    use WxV3PaymentTait;


    /**
     * @param string $orderNo
     * @param int $total
     * @param string|null $openId
     * @param string $payer_client_ip
     * @return array
     * @throws
     */
    #[ArrayShape(['code_url' => "string"])]
    public function payment(string $orderNo, int $total, string $openId = NULL, string $payer_client_ip = '127.0.0.1'): array
    {
        $body = $this->getInitCore($orderNo, $total);

        $sign = $this->signature('POST', '/v3/pay/transactions/native', $json = json_encode($body, JSON_UNESCAPED_UNICODE));

        $client = $this->createClient($sign, $json);
        $client->post('/v3/pay/transactions/native');
        $client->close();

        $json = json_decode($client->getBody(), TRUE);
        if (!isset($json['code_url'])) {
            throw new Exception('微信支付调用失败');
        }
        return ['code_url' => $json['code_url']];
    }


}
