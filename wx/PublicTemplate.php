<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/4/8 0008
 * Time: 9:49
 */

namespace wchat\wx;

use wchat\common\Result;

class PublicTemplate extends SmallProgram
{

    private array  $keywords    = [];
    private string $templateId  = '';
    private array  $first       = [];
    private array  $remark      = [];
    private string $openId      = '';
    private string $defaultUrl  = 'http://weixin.qq.com/download';
    private string $sendUrl     = '/cgi-bin/message/template/send';
    private array  $miniprogram = [];


    /**
     * @param array $keywords
     */
    public function setKeywords(array $keywords): void
    {
        $this->keywords = $keywords;
    }

    /**
     * @param string $templateId
     */
    public function setTemplateId(string $templateId): void
    {
        $this->templateId = $templateId;
    }

    /**
     * @param string $openId
     */
    public function setOpenId(string $openId): void
    {
        $this->openId = $openId;
    }

    /**
     * @param string $defaultUrl
     */
    public function setDefaultUrl(string $defaultUrl): void
    {
        $this->defaultUrl = $defaultUrl;
    }

    /**
     * @param string $name
     * @param string $context
     * @param string $color
     */
    public function replaceKeyword(string $name, string $context, string $color = ''): void
    {
        $this->keywords[$name] = ['value' => $context, 'color' => $color];
    }


    /**
     * @param string $name
     * @param string $context
     * @param null $color
     */
    public function addKeyword(string $name, string $context, $color = null): void
    {
        if (empty($color)) {
            $color = '#000';
        }
        $this->keywords[$name] = [
            'value' => $context,
            'color' => $color
        ];
    }

    /**
     * @param string $context
     * @param string $color
     * @return void
     */
    public function setFirst(string $context, string $color = '#f00'): void
    {
        $this->first = [
            'value' => $context,
            'color' => $color
        ];
    }

    /**
     * @param string $context
     * @param string $color
     * @return void
     */
    public function setRemark(string $context, string $color = '#000'): void
    {
        $this->remark = [
            'value' => $context,
            'color' => $color
        ];
    }

    /**
     * @param string $appid
     * @param string $pagepath
     * @return void
     */
    public function setMiniprogram(string $appid, string $pagepath): void
    {
        $this->miniprogram = [
            'appid'    => $appid,
            'pagepath' => $pagepath
        ];
    }

    /**
     * @return Result
     * @throws
     *
     * 奴隶交易通知
     */
    public function sendTemplate(): Result
    {
        $url                = $this->sendUrl . '?access_token=' . $this->payConfig->getAccessToken();
        $keywords           = $this->keywords;
        $keywords['first']  = $this->first;
        $keywords['remark'] = $this->remark;
        $default            = [
            "touser"      => $this->openId,
            "template_id" => $this->templateId,
            "url"         => $this->defaultUrl,
            "data"        => $keywords,
        ];
        if (!empty($this->miniprogram)) {
            $default['miniprogram'] = $this->miniprogram;
        }
        return $this->post('api.weixin.qq.com', $url, $default);
    }
}
