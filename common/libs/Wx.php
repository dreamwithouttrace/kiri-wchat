<?php

namespace wchat\common\libs;

class Wx
{


    /**
     * @var string
     */
    public string $appId;


    /**
     * @var string
     */
    public string $mchId;


    /**
     * @var string
     */
    public string $schema = 'WECHATPAY2-SHA256-RSA2048';


    /**
     * @var string
     */
    public string $mchKey;


    /**
     * @var string
     */
    public string $secret;


    /**
     * @var string
     */
    public string $mchCert;


    /**
     * @var string
     */
    public string $appSecret;


    /**
     * @var string
     */
    public string $SerialNumber;

}