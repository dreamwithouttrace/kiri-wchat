<?php

namespace wchat\common;

use wchat\common\libs\Aliyun;
use wchat\common\libs\Qq;
use wchat\common\libs\Wx;

class PayConfig
{


    /**
     * @var Qq
     */
    public Qq $qq;


    /**
     * @var Wx
     */
    public Wx $wx;


    /**
     * @var Aliyun
     */
    public Aliyun $ali;


    /**
     * @param string|array $pay
     * @param PayConfig|null $model
     * @return static
     */
    public static function parse(string|array $pay, ?PayConfig $model = null): static
    {
        if (is_string($pay)) {
            $pay = json_decode($pay, true);
        }
        if ($model === null) {
            $model      = new static();
            $model->ali = \Kiri::configure(new Aliyun(), $pay['ali']);
            $model->qq  = \Kiri::configure(new Qq(), $pay['qq']);
            $model->wx  = \Kiri::configure(new Wx(), $pay['wx']);
        } else {
            $model->ali = \Kiri::configure($model->ali, $pay['ali']);
            $model->qq  = \Kiri::configure($model->qq, $pay['qq']);
            $model->wx  = \Kiri::configure($model->wx, $pay['wx']);
        }
        return $model;
    }

}