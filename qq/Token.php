<?php


namespace wchat\qq;


use wchat\common\Result;

class Token extends SmallProgram
{

    /**
     * @return Result
     */
    public function token(): Result
    {
        $query = [
            'grant_type' => 'client_credential',
            'appid'      => $this->payConfig->appId,
            'secret'     => $this->payConfig->appSecret
        ];
        return $this->get('api.q.qq.com', '/api/getToken', $query);
    }

}
