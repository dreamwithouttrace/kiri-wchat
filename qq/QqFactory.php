<?php

namespace wchat\wx;

use Kiri\Di\Container;
use wchat\common\AppConfig;

class QqFactory
{


    /**
     * @param $class
     * @param AppConfig $config
     * @return mixed
     * @throws
     */
    public static function get($class, AppConfig $config): mixed
    {
        $container = Container::instance();
        $object    = $container->get($class);
        $object->setPayConfig($config);
        return $object;
    }


}
